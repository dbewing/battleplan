"""Main file for BattlePlan game!

Sprites courtesy of Kenney "Asset Jesus" Vleugels (kenney.nl)
"""

import arcade
import math
from pyglet import clock
from pyglet import media

SPRITE_SCALING = 0.5

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


class DecayStuff:
    """Fade sprites from a SpriteList, by gradually decreasing 'alpha' value then killing stuff.
       Just add the 'decay_time' property to a sprite on the list and it will be faded out over that
       number of seconds.
    """
    def __init__(self, sprite_list):
        self.sprite_list = sprite_list
        self.alpha_threshold = 0.1  # When alpha drops beneath this, kill sprite.
        self.update_rate = 0.05
        clock.schedule_interval(self.update, self.update_rate)

    def update(self, delta_time):
        """Traverse the sprite_list, decaying designated sprites"""
        for s in list(self.sprite_list):
            if hasattr(s, 'decay_time'):
                # Decay time amounts to N ticks of decay, so multiplier is the Nth root of our kill-threshold
                s.alpha *= math.pow(self.alpha_threshold, self.update_rate / s.decay_time)
                if s.alpha < self.alpha_threshold:
                    s.kill()


class DustTracks:
    """Dust tracks following behind a parent sprite"""
    def __init__(self, ground_sprites, parent_sprite, decay_time=2):
        """Given a ground-level SpriteList and a parent to follow, make tracks!"""
        self.ground_sprites = ground_sprites
        self.parent = parent_sprite
        self.decay_time = decay_time
        self.nearest_dust = None
        clock.schedule_interval(self.update, .03)

        # Start with tracks directly beneath parent
        self.add_track()

    def update(self, delta_time):
        """See if parent has moved far enough to make new tracks"""
        distance = arcade.get_distance_between_sprites(self.nearest_dust, self.parent)
        if distance > self.nearest_dust.width:
            self.add_track()

    def add_track(self):
        """Add a new dust-track sprite beneath parent, and fade old tracks"""
        dust_sprite = arcade.Sprite("images/tracksSmall.png", SPRITE_SCALING)
        dust_sprite.center_x = self.parent.center_x
        dust_sprite.center_y = self.parent.center_y
        dust_sprite.angle = self.parent.angle
        dust_sprite.decay_time = self.decay_time
        self.ground_sprites.append(dust_sprite)
        self.nearest_dust = dust_sprite


class Tank:
    """The basic player object in BattlePlan
       - Updates itself based on elapsed time via 'update' tick.
       - Displays itself by adding sprites to provided lists.
       - Interacts with other players and environment objects TBD...
    """
    def __init__(self, player_sprites, ground_sprites, sky_sprites):
        clock.schedule_interval(self.update, .01)

        # Create a sprite for our body, and add to "player" sprite list
        self.body_sprite = arcade.Sprite("images/tank_sand.png", SPRITE_SCALING)
        player_sprites.append(self.body_sprite)

        # Make some tracks to follow our body
        self.dust_tracks = DustTracks(ground_sprites, self.body_sprite)

        # Hardcode the agility characteristics for now, until we have multiple tank configurations.
        self.turn_max = 5   # degrees per tick @ 100% power
        self.turn_accel = 0.9
        self.drive_max = 9   # pixels per tick @ 100% power
        self.drive_accel = 0.9

        # Power currently being applied to the engines (+-100%)
        self.turn_power = 0
        self.drive_power = 0

        # Current speed at which we're moving
        self.turn_speed = 0    # rotational speed (degrees per tick)
        self.drive_speed = 0   # magnitude of speed in current direction (pixels per tick)

        # Current screen position and orientation
        self.x = SCREEN_WIDTH / 2
        self.y = SCREEN_HEIGHT / 2
        self.angle = 0

        # Load sounds
        self.engine_sound = media.load('sounds/engine1s.wav', streaming=False)
        self.engine_player = media.Player()
        self.engine_player.queue(self.engine_sound)
        self.engine_player.volume = 0
        self.engine_player.play()  # Pre-load so there's no delay on first use

    def move(self, turn_power, drive_power):
        """Move tank by applying turning and drive forces (0-100%)"""
        self.turn_power = turn_power
        self.drive_power = drive_power

    def update_speed(self):
        # Update speed and current position based on applied power and acceleration factor

        # NOTE: Currently only doing deceleration. Positive acceleration is instant!
        if self.turn_power:
            self.turn_speed = self.turn_max * (self.turn_power / 100)
        else:
            self.turn_speed *= self.turn_accel

        if self.drive_power:
            self.drive_speed = self.drive_max * (self.drive_power / 100)
        else:
            self.drive_speed *= self.drive_accel

    def update_position(self):
        # Given a speed and direction, calculate the (x,y) change in position. It's trig!
        direction = -math.radians(self.angle)
        change_x = self.drive_speed * math.sin(direction)
        change_y = self.drive_speed * math.cos(direction)

        self.x += change_x
        self.y += change_y
        self.angle += self.turn_speed

    def update_sound(self):
        # Make engine sound proportional to our current drive/turn speed
        if abs(self.turn_speed) > 1 or abs(self.drive_speed) > 1:
            # Keep engine sound playing. Doesn't loop automatically, so make sure player always has 2 in queue
            if len(self.engine_player._groups) < 2:
                self.engine_player.queue(self.engine_sound)
            if not self.engine_player.playing:
                self.engine_player.play()

            # Set volume and pitch based on current percentage of max drive/turn speed
            drive_pct = abs(self.drive_speed / self.drive_max)
            turn_pct = abs(self.turn_speed / self.turn_max)
            self.engine_player.volume = max(drive_pct, turn_pct)   # Volume min-max is range (0.0, 1.0)
            self.engine_player.pitch = 0.8 + 0.2 * drive_pct  - 0.2 * turn_pct  # Pitch is freq multiplier (e.g. 2.0 => octave up)
        else:
            self.engine_player.pause()

    def update(self, delta_time):
        """Called at fixed 'tick' rate to update tank"""
        self.update_speed()
        self.update_position()
        self.update_sound()
        self.move_sprites()

    def move_sprites(self):
        # Set position of body sprite object
        self.body_sprite.center_x = self.x
        self.body_sprite.center_y = self.y
        self.body_sprite.angle = self.angle


class BattlePlan(arcade.Window):
    """
    Top-level object for the game. Creates all the initial game objects, and has event-handlers
    which are called by the Arcade event loop to move things along.
    """

    def __init__(self, width, height):
        super().__init__(width, height)
        self.set_update_rate(0.01)  # Schedule self.update() call every 10ms

        arcade.set_background_color(arcade.color.AMAZON)

        # Keep track of keys currently being pressed (keyboard state)
        self.keys = {}    # dictionary = {key : isPressed,...}

        self.ground_sprites = arcade.SpriteList()
        self.player_sprites = arcade.SpriteList()
        self.sky_sprites = arcade.SpriteList()

        self.decay = DecayStuff(self.ground_sprites)

        # Create a tank to play with
        self.player1_tank = Tank(self.player_sprites, self.ground_sprites, self.sky_sprites)

    def key_pressed(self, key):
        """Return True if key currently pressed, else False"""
        return self.keys.get(key, False)

    def on_draw(self):
        # This command has to happen before we start drawing
        arcade.start_render()

        # Draw all the sprites.
        self.ground_sprites.draw()
        self.player_sprites.draw()
        self.sky_sprites.draw()

    def update(self, delta_time):
        """Periodic game update() call scheduled by parent arcade.Window class"""
        pass

    def check_keys(self):
        """Update player controls based on current keyboard state"""
        player = self.player1_tank
        power = 100

        if self.key_pressed(arcade.key.UP):
            player.drive_power = -power
        elif self.key_pressed(arcade.key.DOWN):
            player.drive_power = +power
        else:
            player.drive_power = 0

        if self.key_pressed(arcade.key.LEFT):
            player.turn_power = +power
        elif self.key_pressed(arcade.key.RIGHT):
            player.turn_power = -power
        else:
            player.turn_power = 0

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        self.keys[key] = True
        self.check_keys()

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        self.keys[key] = False
        self.check_keys()


if __name__ == "__main__":
    # This is where it all starts. Create the main 'game' object that sets everything up.
    game = BattlePlan(SCREEN_WIDTH, SCREEN_HEIGHT)

    # Now drop into Arcade's "event loop". It will call our game's "update()" function periodically.
    arcade.run()
